#!/bin/sh

if [ "$#" -ne 1 ]; then
	echo "usage: $0 {src-xml-file}"
	exit -1
fi

OLD_PWD=`pwd`
EXEC_FILE="$0"
BASE_NAME=`basename "${EXEC_FILE}"`
if [ "${EXEC_FILE}" = "./${BASE_NAME}" ] || [ "${EXEC_FILE}" = "${BASE_NAME}" ]; then
	FULL_PATH=`pwd`
else
	FULL_PATH=`echo "${EXEC_FILE}" | sed 's/'"${BASE_NAME}"'$//'`
	cd "${FULL_PATH}"                 > /dev/null 2>&1
	FULL_PATH=`pwd`
	cd "${OLD_PWD}"
fi

### Set environment

ROOT="${FULL_PATH}"
BIN_DIR="${FULL_PATH}/bin"

DEST_DIR=`pwd`

TMP_DIR="${ROOT}/tmp"
TMP_RES_DIR="${TMP_DIR}/res"
TMP_LAYOUT_DIR="${TMP_RES_DIR}/layout"
XML_OUTPUT_DIR="${TMP_DIR}/out"
XML_OUTPUT_LAYOUT_DIR="${XML_OUTPUT_DIR}/res/layout"

SRC_XML=$1
AAPT="${BIN_DIR}/aapt"
JAR="/usr/bin/jar"
JAR_OUTPUT="${XML_OUTPUT_DIR}/output.jar"

SRC_FILENAME=`basename $SRC_XML`

### Copy xml into tmp dir
if test -d ${TMP_LAYOUT_DIR}
then
	#do nothing
	echo "#1 [TMP DIR] ${TMP_DIR} exist"
else
	mkdir ${TMP_DIR}
	mkdir ${TMP_RES_DIR}
	mkdir ${TMP_LAYOUT_DIR}
	mkdir ${XML_OUTPUT_DIR}
	echo "#1 [TMP DIR] ${TMP_DIR} created"
	LOG_CNT=${LOG_CNT}+1
fi

cp ${SRC_XML} ${TMP_LAYOUT_DIR}
RSLT=$?
echo "#2 [CP ${RSLT}] copied into ${TMP_LAYOUT_DIR}"
if [ ${RSLT} -ne 0 ]; then
	echo "ERROR!! STOPPED"
	exit -200
fi

### Package

CMD_AAPT="${AAPT} p -f -M ${BIN_DIR}/AndroidManifest.xml -I ${BIN_DIR}/android.jar -S ${TMP_RES_DIR} -F ${JAR_OUTPUT}"
$CMD_AAPT > /dev/null 2>&1
RSLT=$?
echo "#3 [JAR Created ${RSLT} with aapt] ${JAR_OUTPUT}"
if [ ${RSLT} -ne 0 ]; then
	echo "ERROR!! STOPPED"
	exit -300
fi

### Extract jar
cd ${XML_OUTPUT_DIR}

SRC="res/layout/${SRC_FILENAME}"
CMD_JAR="${JAR} -xvf ${JAR_OUTPUT} ${SRC}"
$CMD_JAR > /dev/null 2>&1
RSLT=$?

cd ${OLD_PWD}
echo "#4 [Extracted ${RSLT}]"
if [ ${RSLT} -ne 0 ]; then
	echo "ERROR!! STOPPED"
	exit -400
fi

cp ${XML_OUTPUT_LAYOUT_DIR}/${SRC_FILENAME} .
RSLT=$?
echo "#5 [copyed ${SRC_FILENAME}]"
if [ ${RSLT} -ne 0 ]; then
	echo "ERROR!! STOPPED"
	exit -500
fi

rm -r ${TMP_DIR}

exit 0

#rm -r ${TMP_DIR}


