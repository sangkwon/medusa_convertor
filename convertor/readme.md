Welcome to MEDUSA Layout convertor
----------------------------------

	메두사용 View에서 사용하는 layout xml 파일을 패키징해주는 툴입니다.
	실행한 디렉토리아래에 src 와 같은 이름의 binary xml 을 생성해줍니다.

### Usage
```
	medusa.sh {src-xml-file}
```

### Environment
* Mac / Linux
* JDK installed

